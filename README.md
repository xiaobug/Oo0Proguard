配置Oo0代码混淆，只需要2步：
1，修改Proguard源文件
2，将新的Proguard文件配置给项目

### 开始：
关于修改Proguard源文件的方法和如何导出jar包，请参考[Oo0代码混淆实现方法](https://mp.weixin.qq.com/s/T8CFMjQ5_te1fIHzwzTpUg)。

在项目的根目录创建proguard文件夹，把修改源码后的proguard的jar包复制进来：
![image.png](https://images.gitee.com/uploads/images/2019/0817/114823_8c694bb6_723955.png)

然后在根目录的build.gradle文件中配置引入：
![image.png](https://images.gitee.com/uploads/images/2019/0817/114823_57d39b53_723955.png)

效果：
![image.png](https://images.gitee.com/uploads/images/2019/0817/114823_65109122_723955.png)

如果混淆名称想修改成别的字符集，可以参考ONameFactory中的写法
![image.png](https://images.gitee.com/uploads/images/2019/0817/114823_1dc34342_723955.png)

不想自己编写Proguard源文件的，可以直接[点击下载](https://gitee.com/xiaobug/Oo0Proguard)
![image.png](https://images.gitee.com/uploads/images/2019/0817/114823_a69eb7f9_723955.png)

参考博客，最后感谢原作者的无私奉献：
[Oo0代码混淆实现方法](https://mp.weixin.qq.com/s/T8CFMjQ5_te1fIHzwzTpUg)
[Android Studio自定义proguard混淆](https://blog.csdn.net/u014476720/article/details/85112374)
