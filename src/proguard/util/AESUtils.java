package proguard.util;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * AES加密解密工具类
 */
public class AESUtils {
    /**
     * 加密
     *
     * @param data
     * @param password
     * @return
     */
    public static String encrypt(String data, String password) {
        try {
            //创建AES的key生成器
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            //初始化生成器
            keyGenerator.init(128, new SecureRandom(password.getBytes(Charset.forName("utf-8"))));
            //生成秘钥
            SecretKey secretKey = keyGenerator.generateKey();
            //返回基本编码格式的秘钥
            byte[] encoded = secretKey.getEncoded();
            //转换为AES专用秘钥
            SecretKeySpec secretKeySpec = new SecretKeySpec(encoded, "AES");
            //创建密码器
            Cipher cipher = Cipher.getInstance("AES");
            //初始化为加密模式的密码器
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            //加密
            byte[] bytes = cipher.doFinal(data.getBytes(Charset.forName("utf-8")));
            //base64编码
            return new BASE64Encoder().encodeBuffer(bytes);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 解密
     *
     * @param data
     * @param password
     * @return
     */
    public static String decrypt(String data, String password) {
        try {
            //创建AES的key生成器
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            //初始化生成器
            keyGenerator.init(128, new SecureRandom(password.getBytes(Charset.forName("utf-8"))));
            //生成秘钥
            SecretKey secretKey = keyGenerator.generateKey();
            //返回基本编码格式的秘钥
            byte[] encoded = secretKey.getEncoded();
            //转换为AES专用秘钥
            SecretKeySpec secretKeySpec = new SecretKeySpec(encoded, "AES");
            //创建密码器
            Cipher cipher = Cipher.getInstance("AES");
            //初始化为解密模式的密码器
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            //进行base64解码
            byte[] bytes = new BASE64Decoder().decodeBuffer(data);
            //解密
            byte[] result = cipher.doFinal(bytes);
            return new String(result, "utf-8");

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
